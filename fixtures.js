const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Category = require('./models/Category');
const Product = require('./models/Product');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('products');
    await db.dropCollection('categories');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [user1, user2] = await User.create({
    username: 'john_doe',
    password: 'john',
    displayName: 'John Doe',
    phone: '+996 555050505'
  }, {
    username: 'black_jack',
    password: 'jack',
    displayName: 'Jack Black',
    phone: '+996 557070707'
  });

  const [computersCategory, carsCategory, otherCategory] = await Category.create({
    title: 'Computers',
    description: 'Computers & Notebooks'
  }, {
    title: 'Cars',
    description: 'Cars'
  }, {
    title: 'Other',
    description: 'Other products'
  });

  await Product.create({
    seller: user1._id,
    category: computersCategory._id,
    title: 'Computer',
    description: 'Computer Intel Core i7 MSI Nightblade',
    price: 850,
    image: 'MSI_Nightblade.jpg'
  }, {
    seller: user1._id,
    category: carsCategory._id,
    title: 'Audi',
    description: 'Audi RS6 2008',
    price: 32000,
    image: 'Audi_RS6.jpg'
  }, {
    seller: user1._id,
    category: otherCategory._id,
    title: 'Router',
    description: 'Wi-Fi router Tenda W309R',
    price: 20,
    image: 'Tenda_W309R.jpg'
  }, {
    seller: user2._id,
    category: computersCategory._id,
    title: 'Notebook',
    description: 'Notebook ASUS RoG GL702VM',
    price: 920,
    image: 'ASUS_ROG.jpg'
  }, {
    seller: user2._id,
    category: carsCategory._id,
    title: 'Sprinter',
    description: 'Mersedes-Benz Sprinter 2008',
    price: 12000,
    image: 'Sprinter.jpg'
  }, {
    seller: user2._id,
    category: otherCategory._id,
    title: 'Router',
    description: 'Wi-Fi router TP-Link TL-VR940N',
    price: 20,
    image: 'TL-VR940N.jpg'
  });

  db.close();
});