const express = require('express');
const Category = require('../models/Category');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Category.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;