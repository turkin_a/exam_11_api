const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const Product = require('../models/Product');
const User = require('../models/User');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});
const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    if (req.query.category) {

      Product.find({category: req.query.category})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));

    } else {

      Product.find()
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }
  });

  router.get('/:id', (req, res) => {
    Product.findById(req.params.id)
      .populate([{path: 'seller', select: ['displayName', 'phone']}, {path: 'category', select: 'title'}])
        .then(results => {
          res.send(results);
        })
        .catch(() => res.sendStatus(500));

  });

  router.post('/', upload.single('image'), (req, res) => {
    const productData = req.body;

    if (req.file) {
      productData.image = req.file.filename;
    } else {
      productData.image = null;
    }

    const product = new Product(productData);

    product.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.delete('/:id', async (req, res) => {
    const token = req.get('Token');

    const user = await User.findOne({token: token});

    const product = await Product.findById(req.params.id);

    if (user._id.toString() === product.seller.toString()) {
      Product.findByIdAndRemove(req.params.id)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    }
    else {
      res.status(400).send({message: 'Authentication error'});
    }
  });

  return router;
};

module.exports = createRouter;